public class Converter {
    public static double convertCelsiusToFarenheit(double celsius) {
        return (celsius * 9.0 / 5.0) + 32.0;
    }

    public static double convertFahrenheitToCelsius (double fahr) {
        return (fahr - 32.0) * 5.0/9.0;
        
    }

    public static void printCelsiusTable(int start, int end, int step){
        System.out.println("Celsius Farenheit");
        for (int i = start; i<=end; i = i + step){
            System.out.println("" + i + "" +
                convertCelsiusToFarenheit(i));
        }
    }

    public static void printFahrenheitTable (int start, int end, int step) {
        System.out. println(" Fahrenheit Celsius ");
        for (int i = start; i <= end; i = i + step) {
            System.out.println (" " + i + " " + convertFahrenheitToCelsius(i));
            
        }
    }

    // Solution to part 1
    public static void main (String[] args) {
        if (args.length == 3) {
            try {
                int start = Integer.parseInt (args [0]);
                int end = Integer.parseInt (args [1]);
                int step = Integer.parseInt (args [2]);
                printFahrenheitTable(start, end, step);
            } catch (NumberFormatException ex) {
                System.out.println ("Error: you need to provide 3 numbers");
            }
        } else {
            System.out.println(" Wrong number of arguments ");
        }
    }

    /*
    // Solution to part 3
    public static void main(String[] args) {
        if (args.length == 1) {
        // *** We simply check for the case        of a single arg
        int fahr = Integer.parseInt (args[0]) ;
        System.out.println ("Fahrenheit " + fahr + " = Celsius " +
        convertFahrenheitToCelsius(fahr));
        } else if (args.length == 3) {
        int start = Integer.parseInt (args[0]);
        int end = Integer.parseInt (args[1]);
        int step = Integer.parseInt (args[2]);
        printFahrenheitTable (start, end, step);
        } else {
        System.out.println ("Wrong number of arguments");
        }
        }

    /*
    // Solution to part 5
    public static void main(String[] args) {
        if(args.length == 2){//IF THE INPUT NUMBER OF COMMANDS IS 2, THEN
            //THERE WILL BE ONLY 2 OPTIONS, F TO C, OR C TO F
            double value = Integer.parseInt (args[1]);
            if ("F".equals(args[0])){
                System.out.println("Farenheit" + value + " = Celsius " +
                convertFahrenheitToCelsius(value));
            } else {
                System.out.println("Celsius " + value + " = Fahrenheit " +
                convertCelsiusToFarenheit(value));
            }

        } else if (args.length == 4) {//IF THE INPUT NUMBER OF COMMANDS IS 4, THEN
            //THERE WILL BE 2 OPTIONS, F TO C, OR C TO F
            int start = Integer.parseInt (args[1]);
            int end = Integer.parseInt (args[2]);
            int step = Integer.parseInt (args[3]);
            if("F".equals(args[0])){
                printFahrenheitTable (start, end, step);
            }else {
                printCelsiusTable (start, end, step);
            }
        } else {
            System.out.println("Wrong number of arguments");
        }
    
        /*
        // Initial exercise Unit 1
        }
        double smallest = Double.parseDouble(args[0]);
        double largest = Double.parseDouble(args[1]);
        for (double i = smallest; i <= largest; ++i) {
            System.out.print(i + " in fahrenheit = ");
            System.out.format("%.2f", convertCelsiusToFarenheit(i));
            System.out.println();
        }*/
    
}